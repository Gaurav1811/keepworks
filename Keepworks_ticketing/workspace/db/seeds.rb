20.times do |n|
  name  = "Event "+ n.to_s
 # name  = Faker::Name.name
  fees = rand(1000)
 	event_dates = Faker::Date.between(20.days.ago, 30.days.from_now)
 	create_date = Faker::Date.between(10.days.ago, 0.days.from_now)
  Event.create(name:  name,
               date_of_event: event_dates,
               created_at:create_date,
               fee: fees
                )
end