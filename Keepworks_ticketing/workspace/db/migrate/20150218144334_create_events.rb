class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :date_of_event
      t.integer :fee

      t.timestamps null: false
    end
  end
end
