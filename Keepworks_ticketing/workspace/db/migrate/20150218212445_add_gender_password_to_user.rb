class AddGenderPasswordToUser < ActiveRecord::Migration
  def change
     add_column :users, :password, :string
     add_column :users, :gender, :string
  end
end
