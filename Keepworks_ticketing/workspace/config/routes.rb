Rails.application.routes.draw do

  root 'events#index'
  get 'register'  => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get 'createnewevent' => 'events#createnewevent'
  get 'showevent' => 'events#show'
  resources :events
  resources :users
  
 # post 'events/id' => 'events#create'
  

end