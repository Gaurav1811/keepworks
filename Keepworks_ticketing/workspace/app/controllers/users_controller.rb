class UsersController < ApplicationController
   
  before_action :logged_in_user, only: [:edit, :update, :show]
  before_action :correct_user, only: [:edit, :update, :show]
  def new
    @user = User.new
  end

  def index
    redirect_to(current_user)
  end
  def show
    @user = User.find(params[:id])
  end
  

	def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      redirect_to @user
    else
      render 'new'
    end
  end

  private

  	def user_params
    		params.require(:user).permit(:name, :email, :password, :password_confirmation, :gender)
  	end

    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in"
        redirect_to login_url
      end
    end
    def correct_user
      @user = User.find(params[:id])
      redirect_to(current_user) unless current_user?(@user)
    end
end
